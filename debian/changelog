rust-leptess (0.14.0-9) unstable; urgency=medium

  * add metadata about upstream project
  * update git-buildpackage config:
    + filter out debian subdir
    + simplify usage comments
  * update watch file:
    + improve filename mangling
    + use Github API
    + drop unused upstream version mangling
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 07 Feb 2025 10:42:11 +0100

rust-leptess (0.14.0-8) unstable; urgency=medium

  * tighten build- and autopkgtest-dependencies for crate image

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 11 Nov 2024 17:32:40 +0100

rust-leptess (0.14.0-7) unstable; urgency=medium

  * update patch 1001 to avoid obsolete image::ImageOutputFormat;
    closes: bug#1087316, thanks to Peter Green

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 11 Nov 2024 17:19:36 +0100

rust-leptess (0.14.0-6) unstable; urgency=medium

  * stop mention dh-cargo in long description
  * add patch 1001_image to accept newer branch of crate image;
    relax build- and autopkgtest-dependencies for crate image;
    closes: bug#1086993, thanks to Matthias Geiger

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 07 Nov 2024 23:51:40 +0100

rust-leptess (0.14.0-5) unstable; urgency=medium

  * really fix autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 21 Jul 2024 02:07:58 +0200

rust-leptess (0.14.0-4) unstable; urgency=medium

  * autopkgtest-depend on dh-rust (not dh-cargo)
  * fix project version in autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 20 Jul 2024 17:54:29 +0200

rust-leptess (0.14.0-3) unstable; urgency=medium

  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)
  * declare compliance with Debian Policy 4.7.0

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Jul 2024 14:29:49 +0200

rust-leptess (0.14.0-2) unstable; urgency=medium

  * no-changes source-only upload to enable testing migration

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 06 Apr 2024 23:52:55 +0200

rust-leptess (0.14.0-1) experimental; urgency=medium

  * initial packaging release;
    closes: bug#1067574

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 23 Mar 2024 22:49:30 +0100
